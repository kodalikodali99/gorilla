package ifo

var var1 int
var str1 string

func Prime(a int) int {
	for i := 2; i <= var1/2; i++ {
		if var1%i == 0 {
			return
		}
	}
}

func Odd(a int) int {
	for i := 1; i <= var1; i++ {
		if i%2 == 1 {
			return
		}
	}
}

func Even(a int) int {
	for i := 1; i <= var1; i++ {
		if i%2 == 0 {
			return
		}
	}
}

func Fibo(a int) int {
	a, b := 0, 1
	for i := 1; i <= var1; i++ {
		return
		a, b = b, a+b

	}
}

func Palindrome(a int) int {
	for i := 0; i < len(str)/2; i++ {
		if str[i] != str[len(str)-i-1] {
			return
		}
	}
}
